package observer

import (
	"fmt"
	"time"

	"main/scenario"
)

type Observer struct {
	scenario   scenario.Scenario
}

func NewObserver() Observer {
	observer := Observer{}
	return observer
}

func (observer *Observer) Start(pathToScenarioFile string) {
	observer.scenario = scenario.NewScenario(pathToScenarioFile)
	observer.scenario.Init()
	defer observer.scenario.End()

	//TODO リファクタ
	for {
		publish(observer)
		time.Sleep(1 * time.Second)
	}
}

func publish(observer *Observer) {
	observationRecord := observer.scenario.Next()

	fmt.Println(observationRecord)
}
