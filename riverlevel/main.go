package main

import (
	"fmt"
	"os"
	"time"

	"main/observer"
)

func main() {

	podName := os.Getenv("POD_NAME")

	fmt.Printf("river-level start. podName=%s\n", podName)

	scenarioFile := fmt.Sprintf("./scenario_data/%s.csv", podName)

	obs := observer.NewObserver()
	go obs.Start(scenarioFile)

	for  {
		time.Sleep(100 * time.Millisecond)
	}

	fmt.Println("rive-level end.")
}
