package scenario

import (
	"encoding/csv"
	"io"
	"os"
)

type Scenario struct {
	FilePath       string
	scenarioFile   *os.File
	scenarioReader *csv.Reader
}

type ObservationRecord struct {
	Location        string
	ObservationTime string
	ObservedValue   string
}

func NewScenario(pathToScenarioFile string) Scenario {
	scenario := Scenario{}
	scenario.FilePath = pathToScenarioFile
	return scenario
}

func makeRecord(line []string) ObservationRecord {
	observationRecord := ObservationRecord{}

	observationRecord.Location = line[0]
	observationRecord.ObservationTime = line[1]
	observationRecord.ObservedValue = line[2]

	return observationRecord
}

func (scenario *Scenario) Init() {
	file, err := os.Open(scenario.FilePath)
	if err != nil {
		panic(err)
	}

	scenario.scenarioFile = file
	scenario.scenarioReader = csv.NewReader(file)
	_, _ = scenario.scenarioReader.Read() //skip header
}

func (scenario *Scenario) End() {
	err := scenario.scenarioFile.Close()
	if err != nil {
		panic(err)
	}
}

func (scenario *Scenario) Next() ObservationRecord {
	line1, err := scenario.scenarioReader.Read()
	if err == nil {
		return makeRecord(line1)
	}

	// rewind
	scenario.scenarioFile.Seek(0, io.SeekStart)
	scenario.scenarioReader.Read() //skip header
	line2, _ := scenario.scenarioReader.Read()

	return makeRecord(line2)
}
